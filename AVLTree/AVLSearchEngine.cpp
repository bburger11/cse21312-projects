/**********************************************
* File: AVLSearchEngine.cpp
* Author: Benjamin Burger, Connor Laracey, Dmitri Wolffe, Laurence Caradonna
* Email: bburger@nd.edu, claracey@nd.edu, dwolff1@nd.edu, Laurence.J.Caradonna.3@nd.edu
* 
* This program implements a basic search engine using an AVL tree
**********************************************/
#include <iostream>
#include <string>
#include <iomanip>
#include "AVLTree.h"


struct Website {
    std::string webName;
    std::string url;
    Website(std::string webName, std::string url) : webName(webName), url(url) {}
    bool operator<(const Website& rhs) const{
        if (url < rhs.url)
            return true;
        return false;
    }
    bool operator>(const Website& rhs) const{
        if (url > rhs.url)
            return true;
        return false;
    }
    bool operator==(const Website& rhs) const{
        if (url == rhs.url)
            return true;
        return false;
    }
    friend std::ostream& operator<<(std::ostream& outStream, const Website& printSite);
};

std::ostream& operator<<(std::ostream& outStream, const Website& printSite) {
    outStream << setw(25) << left << printSite.webName << printSite.url;
    return outStream;
}

void InsertAndPrint(Website*, AVLTree<Website>*);

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
* 
* Main driver to demonstrate website struct and AVL tree manip 
********************************************/
int main(int argc, char **argv) {
    AVLTree<Website> avlWebsite;
    Website website1 ("YouTube", "https://www.youtube.com");
    Website website2 ("Reddit", "https://www.reddit.com");
    Website website3 ("ND CSE Homepage", "https://www.cse.nd.edu");
    Website website4 ("Twitch", "https://www.twitch.tv");
    Website website5 ("Chegg", "https://www.chegg.com");
    Website website6 ("CBS Sports", "https://www.cbssports.com");
    Website website7 ("WebAssign", "https://www.webassign.net");
    Website website8 ("C++ Reference", "https://www.cplusplus.com/reference/");
    Website website9 ("Apple", "https://www.apple.com");
    Website website10("Gitlab", "https://www.gitlab.com");
   
    // Insert and print is a small helper function to visualize the tree
    InsertAndPrint(&website1, &avlWebsite);
    InsertAndPrint(&website2, &avlWebsite);
    InsertAndPrint(&website3, &avlWebsite);
    InsertAndPrint(&website4, &avlWebsite);
    InsertAndPrint(&website5, &avlWebsite);
    InsertAndPrint(&website6, &avlWebsite);
    InsertAndPrint(&website7, &avlWebsite);
    InsertAndPrint(&website8, &avlWebsite);
    InsertAndPrint(&website9, &avlWebsite);
    InsertAndPrint(&website10, &avlWebsite);
    
    // Remove the root and print the tree
    std::cout << "Removing Reddit (root) from tree:" << std::endl;
    std::cout << "---------------------------------" << std:: endl;
    avlWebsite.remove(website2);
    avlWebsite.printTree();
    return 0;
}

/********************************************
* Function Name  : InsertAndPrint
* Pre-conditions : Website *w, AVLTree<Website> *t
* Post-conditions: none
* 
* Helper function to perform insert and print operations for the website 
********************************************/
void InsertAndPrint(Website *w, AVLTree<Website> *t) {
    // Insert the website into the AVLTree
    t->insert(*w);

    // Print information
    int l = w->webName.length();
    std::cout << "Tree After Inserting: " << w->webName << std::endl;
    for (int i = 1; i <= 22 + l; i++)
        std::cout << "-";
    std::cout << endl;

    // Print the tree
    t->printTree();
    std::cout << endl;
}
