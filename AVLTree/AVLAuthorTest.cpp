/**********************************************
* File: AVLAuthorTest.cpp
* Author: Ben Burger
* Email:  bburger@nd.edu
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"


// Struct goes here
struct Author {
    std::string firstName;
    std::string lastName;
    Author(std::string firstName, std::string lastName) : firstName(firstName), lastName(lastName) {}
    bool operator<(const Author& rhs) const{
        if (lastName < rhs.lastName)
            return true;
        else if (lastName == rhs.lastName) {
            if (firstName < rhs.firstName)
                return true;
        }
        return false;
    }
    bool operator==(const Author& rhs) const{
        if (lastName != rhs.lastName)
            return false;
        else {
            if (firstName != rhs.firstName)
                return false;
        }
        return true;
    }
    friend std::ostream& operator<<(std::ostream& outStream, const Author& printAuth);
};

std::ostream& operator<<(std::ostream& outStream, const Author& printAuth) {
    outStream << printAuth.lastName << ", " << printAuth.firstName;
    return outStream;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
    AVLTree<Author> avlAuthor;
    Author Aardvark1("Aaron", "Aardvark");
    Author Aardvark2("Greg", "Aardvark");
    Author Aardvark3("Badmean", "Aardvark");
    Author Aardvark4("Longfellow", "Deeds");
    Author Aardvark5("Jerry", "Paper");
    
    avlAuthor.insert(Aardvark1);
    avlAuthor.insert(Aardvark2);
    avlAuthor.insert(Aardvark3);
    avlAuthor.insert(Aardvark4);
    avlAuthor.insert(Aardvark5);
    

    std::cout << "Original Tree:" << std::endl;
    std::cout << "--------------" << std::endl;
    avlAuthor.printTree();

    std::cout << "Remove BadMean:" << std::endl;
    std::cout << "---------------" << std::endl;

    avlAuthor.remove(Aardvark3);

    avlAuthor.printTree();
    
    return 0;
}





