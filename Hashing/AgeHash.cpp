/**********************************************
* File: AgeHash.cpp
* Author: Benjamin Burger
* Email: bburger@nd.edu
*  
* Implements a basic hash table based on names 
* and ages
**********************************************/
#include <map>
#include <unordered_map>
#include <iterator>
#include <string>
#include <iostream>
#include <iomanip>

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
* The main driver for this program
********************************************/
int main(int argc, char** argv) {
    
    // declare ordered map and map iterator
    std::map<std::string, int> ageHashOrdered = { {"Matthew", 38}, {"Alfred", 72}, {"Roscoe", 36}, {"James", 38} };
    std::map<std::string, int>::iterator iterOr;
    
    // output ordered
    std::cout << "The ordered ages are: " << std::endl;
    for (iterOr = ageHashOrdered.begin(); iterOr != ageHashOrdered.end(); iterOr++) {
        std::cout << "Key: " << std:: setw(10) << std::left << iterOr->first << " Value: " << iterOr->second << std::endl;
    }
    std::cout << std::endl;
    
    // declare unordered map and unordered map iterator
    std::unordered_map<std::string, int> ageHashUnordered = { {"Matthew", 38}, {"Alfred", 72}, {"Roscoe", 36}, {"James", 38} };
    std::unordered_map<std::string, int>::iterator iterUn;
    
    // output ordered
    std::cout << "The unordered ages are: " << std::endl;
    for (iterUn = ageHashUnordered.begin(); iterUn != ageHashUnordered.end(); iterUn++) {
        std::cout << "Key: " << std:: setw(10) << std::left << iterUn->first << " Value: " << iterUn->second << std::endl;
    }
    std::cout << std::endl;
    
    // element access
    std::cout << "The ordered example:   " << ageHashOrdered["Matthew"] << std::endl;
    std::cout << "The unordered example: " << ageHashUnordered["Alfred"] << std::endl;
    
    return 0;
}
