/**********************************************
* File: AgeHash.cpp
* Author: Benjamin Burger
* Email: bburger@nd.edu
*  
* Implements a basic hash table based on names 
* and ages
**********************************************/
#include <map>
#include <unordered_map>
#include <iterator>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>

/********************************************
* Function Name  : usage
* Pre-conditions : int status
* Post-conditions: none
* 
* Usage function
********************************************/
void usage(int status) {
    std::cout << "Usage: " << "./WordCountHash FILE" << std::endl;
    exit(status);
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
* The main driver for this program
********************************************/
int main(int argc, char** argv) {
    // parse arguments, open book
    if (argc != 2) {
        usage(1);
    }
    std::ifstream book;
    book.open(argv[1]);
    if (!book.is_open()) {
        std::cout << "Error reading file " << argv[1] << "." << std::endl;
        exit(1);
    }
    
    // declare ordered map and map iterator
    std::map<std::string, int> wordHashList = { }; 
    std::map<std::string, int>::iterator iter;
    
    // read each word of book
    std::string word;
    while (book >> word) {
        // loop through word, remove all special characters
        for(int i = 0; i < word.size(); ++i) {
            if (!((word[i] >= 'a' && word[i]<='z') || (word[i] >= 'A' && word[i]<='Z'))) {
                word[i] = '\0';
            }
        }
        // to lower
        std::transform(word.begin(), word.end(), word.begin(), ::tolower);
        // find it
        iter = wordHashList.find(word);
        // if found, increment
        if (iter != wordHashList.end()) {
            wordHashList[word]++;    
        // if not, set to one
        } else {
            wordHashList[word] = 1;
        }
    }
    // loop through, print results
    for (iter = wordHashList.begin(); iter != wordHashList.end(); iter++) {
        std::cout << std::setw(15) << std::left << iter->first << std::setw(8) << std::left << "=>" << iter->second << std::endl;
    }
    std::cout << std::endl;

    return 0;
}
